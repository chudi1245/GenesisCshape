﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Genesis;

namespace copper
{
    public partial class frm_main : Form
    {
        private gCOM gc;
        private gGetInfo gi;
        private double min_bru;
        private double copper_distance;
        private double via_ring;
        private double plate_ring;

        public frm_main()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                min_bru = double.Parse(textBox1.Text);
                copper_distance = double.Parse(textBox2.Text);
                via_ring = double.Parse(textBox3.Text) * 1000;
                plate_ring = double.Parse(textBox4.Text) * 1000;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            doit();
            Application.Exit();
        }

        private void doit()
        {
            gc = new gCOM();
            gi = new gGetInfo();
            string layers = gi.getWorklayer();
            string target_layers = layers + "ccc++++";

            gc.sel_move_other(target_layer: target_layers);
            //运行检测
            gc.chklist_single(show:"no");
            gc.chklist_cupd(layers, plate_ring.ToString(), via_ring.ToString());
            gc.chklist_run();
            //开始掏铜
            gc.display_layer(layers: target_layers,number:3);
            gc.sel_ref_feat(layers: target_layers);
            gc.sel_copy_other(target_layer: target_layers, invert: "yes", size: copper_distance * 2 * 1000);
            gc.work_layer(target_layers);
            gc.getWorklayer();
            gc.disp_on();
            gc.origin_on();
            gc.sel_contourize();
            gc.fill_params(min_brush: min_bru * 1000);
            gc.sel_fill();
            gc.sel_contourize();
            gc.sel_move_other(target_layer: layers);
            MessageBox.Show("完成！");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            gc.PAUSE();
        }
    }
}
