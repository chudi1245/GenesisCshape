﻿/*
 * 封装了常用的获取信息的命令
 * 褚迪  2017-8-22
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Genesis
{
    public class gGetInfo : Genesis
    {
        /// <summary>
        /// 获取JOB列表
        /// </summary>
        public string[] getJOBList()
        {
            DO_INFO(entity_type: "root", data_type: "JOBS_LIST");
            try
            {
                //如果返回数组没有异常的话则直接返回
                return (string[])getInfo("gJOBS_LIST");
            }
            catch (Exception)
            {
                //抓取错误之后返回数组；
                string[] s = { (string)getInfo("gJOBS_LIST") };
                return s;
            }

        }

        /// <summary>
        /// 判断层是否存在
        /// </summary>
        /// <param name="job"></param>
        /// <param name="step"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public bool isLayerExits(string job,string step,string layer)
        {
            DO_INFO(entity_type: "layer", entity_path: $"{job}/{step}/{layer}",
                data_type:"EXISTS");
            if((string) getInfo("gEXISTS") == "yes")
            {
                return true;
            }else
            {
                return false;
            }
        }
        /// <summary>
        /// 判断step是否存在
        /// </summary>
        /// <param name="job"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public bool isStepExits(string job,string step)
        {
            DO_INFO(entity_type: "step", entity_path: $"{job}/{step}",
               data_type: "EXISTS");
            if ((string) getInfo("gEXISTS") == "yes")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 判断JOB是否存在
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool isJobExits(string job)
        {
            DO_INFO(entity_type: job, entity_path: job, data_type: "EXISTS");
            if ((string) getInfo("gEXISTS") == "yes")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获取行矩阵信息
        /// </summary>
        public ArrayList getMatrixRow(string job)
        {
            DO_INFO(entity_type: "matrix", entity_path: $"{job}/{"matrix"}",
                data_type: "ROW");
            ArrayList ROW = new ArrayList();
            //获取所有参数
            string[] rows = (string[])getInfo("gROWrow");
            string[] types = (string[])getInfo("gROWtype");
            string[] names = (string[])getInfo("gROWname");
            string[] contexts = (string[])getInfo("gROWcontext");
            string[] layer_types = (string[])getInfo("gROWlayer_type");
            string[] layer_base_types = (string[])getInfo("gROWlayer_base_type");
            string[] polaritys = (string[])getInfo("gROWpolarity");
            string[] sides = (string[])getInfo("gROWside");
            string[] drl_starts = (string[])getInfo("gROWdrl_start");
            string[] drl_ends = (string[])getInfo("gROWdrl_end");
            string[] foil_sides = (string[])getInfo("gROWfoil_side");
            string[] sheet_sides = (string[])getInfo("gROWsheet_side");

            for (int i = 0; i < rows.Length; i++)
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data["row"] = rows[i];
                data["type"] = types[i];
                data["name"] = names[i];
                data["context"] = contexts[i];
                data["layer_type"] = layer_types[i];
                data["layer_base_type"] = layer_base_types[i];
                data["polarity"] = polaritys[i];
                data["side"] = sides[i];
                data["drl_start"] = drl_starts[i];
                data["drl_end"] = drl_ends[i];
                data["foil_side"] = foil_sides[i];
                data["sheet_side"] = sheet_sides[i];
                ROW.Add(data);
            }
            return ROW;
        }

        /// <summary>
        /// 获取STEP的尺寸
        /// </summary>
        public SizeF getStepLIMITS(string job, string step, string u = "mm")
        {
            SizeF limits = new SizeF();
            DO_INFO(entity_type: "step", entity_path: $"{job}/{step}",
                data_type: "PROF_LIMITS", units: u);
            limits.Width = (float)getInfo("gPROF_LIMITSxmax") - (float)getInfo("gPROF_LIMITSxmin");
            limits.Height = (float)getInfo("gPROF_LIMITSymax") - (float)getInfo("gPROF_LIMITSymin");
            return limits;
        }
        /// <summary>
        /// 获取用户名称
        /// </summary>
        public string getUserName()
        {
            COM("get_user_name");
            return COMANS;
        }

        /// <summary>
        /// 获取工作层
        /// </summary>
        public string getWorklayer()
        {
            COM("get_work_layer");
            return COMANS;
        }

        /// <summary>
        /// 获取rout层
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public ArrayList get_rout_layers(string job)
        {
            return get_Layers(job, layer_type: "rout");
        }
        
        public ArrayList get_signal_layers(string job)
        {
            return get_Layers(job, layer_type: "signal");
        }

        public ArrayList get_solder_mask_layers(string job)
        {
            return get_Layers(job, layer_type: "solder_mask");
        }

        public ArrayList get_silk_screen_layers(string job)
        {
            return get_Layers(job, layer_type: "silk_screen");
        }
        public ArrayList get_outer_layers(string job)
        {
            ArrayList outer_layer = new ArrayList();
            foreach(Dictionary<string,string> item in get_signal_layers(job))
            {
                if(item["side"]=="top" || item["side"] == "bottom")
                {
                    outer_layer.Add(item);
                }
            }
            return outer_layer;

        }
        public ArrayList get_board_layers(string job)
        {
            return get_Layers(job);
        }

        public ArrayList get_inner_layers(string job)
        {
            ArrayList inner_layer = new ArrayList();
            foreach (Dictionary<string, string> item in get_signal_layers(job))
            {
                if (item["layer_type"] == "signal" || item["layer_type"] == "power_ground" || item["layer_type"] == "mixed")
                {
                    inner_layer.Add(item);
                }
            }
            return inner_layer;
        }

        private ArrayList get_Layers(string job,string layer_type=null,string context = "board")
        {
            ArrayList tmpList = getMatrixRow(job);
            ArrayList layer_list = new ArrayList();
            foreach(Dictionary<string,string> item in tmpList)
            {
                //如果layer_type没有设置的话，则只返回context属性的层
                if (layer_type == null)
                {
                    if(item["context"] == context)
                    {
                        layer_list.Add(item);
                    }else if(item["layer_type"] == layer_type && item["context"] == context)
                    {
                        layer_list.Add(item);
                    }
                }
            }
            return layer_list;
        }
    }
}
